const MATRIX_WRONG_MOCKS = {
    'MATRIX_4X4_WRONG': [
        'A,C,A,A',
        'C,A,T,G',
        'C,C,T,G',
        'G,T,G,A'
    ],

    'MATRIX_6X6_WRONG': [
        'A,A,C,A,G,C',
        'C,A,T,G,T,G',
        'A,T,T,G,T,A',
        'G,T,G,A,C,C',
        'T,A,G,A,C,A',
        'C,C,T,C,T,G',
    ],
}

module.exports={
    MATRIX_WRONG_MOCKS
}