const MATRIX_OK_MOCKS = {
    'MATRIX_4X4_ROW_MATCH': [
        'A,A,A,A',
        'C,A,T,G', 
        'C,C,T,G',
        'G,T,G,A'
    ],
    'MATRIX_4X4_COLUMN_MATCH': [
        'A,T,T,A',
        'G,A,T,G',
        'C,T,T,G',
        'G,T,T,A'
    ],
    'MATRIX_4X4_MAIN_DIAGONAL_MATCH': [
        'T,T,A,A',
        'G,T,T,G',
        'C,G,T,G',
        'G,A,A,T'
    ],
    'MATRIX_4X4_SEC_DIAGONAL_MATCH': [
        'G,T,A,A',
        'G,T,A,G',
        'C,A,G,G',
        'A,A,T,T'
    ],

    'MATRIX_6X6_ROW_MATCH': [
        'A,A,C,A,G,C',
        'C,A,T,G,T,G',
        'A,T,T,T,T,A',
        'G,T,G,A,C,C',
        'T,A,G,A,C,A',
        'C,C,T,C,T,G',
    ],
    'MATRIX_6X6_COLUMN_MATCH': [
        'A,A,C,A,G,C',
        'C,A,T,G,A,G',
        'A,G,T,A,A,A',
        'G,T,G,T,A,C',
        'T,A,G,A,A,A',
        'C,C,T,T,T,G',
    ],
    'MATRIX_6X6_DIAGONAL_10_43_MATCH':[
        'A,A,C,A,G,C',
        'C,A,T,G,A,G',
        'A,C,T,A,C,A',
        'G,T,C,T,A,C',
        'T,A,G,C,C,A',
        'C,C,T,C,T,G',
    ],
    'MATRIX_6X6_DIAGONAL_42_51_MATCH':[
        'A,A,T,G,A,G',
        'A,C,T,A,G,A',
        'A,A,C,G,G,C',
        'G,T,G,G,A,C',
        'T,A,G,T,C,A',
        'C,C,T,C,T,G',
    ],
}

module.exports={
    MATRIX_OK_MOCKS
}