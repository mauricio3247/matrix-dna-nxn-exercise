require('dotenv').config()
const express = require('express')
const cors = require ('cors')
const helmet = require('helmet')

const routerMutation = require('./controllers/mutation')
const routerStats = require('./controllers/stats')

const mongoose = require('mongoose')

mongoose.connect(process.env.MONGO_CONNECTION, { useUnifiedTopology: true,  useNewUrlParser: true, useCreateIndex: true  } )
const app = express()

app.use(cors())
app.use(helmet())
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(routerMutation)
app.use(routerStats)

module.exports = app

