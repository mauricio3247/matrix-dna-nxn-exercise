# MATRIX DNA NxN

This project allow to evaluate a dna matrix nxn and check if the matrix have or not any mutation. A mutation exist when we have a sequence of four letter in a row, column or diagonal, also the result are storaged in db for help to another service developed to expose stats of the dnas evaluated, the stat service shows how many mutations were found, how many no-mutations were found and mutations ratio 

## Get Started 🚀

### Prerequisites
Before start you need:

```
you need install node ^12, npm ^6.14
mongodb connection (atlas, local, etc)
set your own .env file using .env-example
run command: npm i -g mocha
```

### Instalation
using the terminal inside folder project, run command
```
npm install
```

### Running app
using the terminal inside folder project, run command
```
npm run start
```

## Running tests ⚙️
The project use mocha and chai to unit tests, to run the unit tests, your need run the the command 
```
npm run test
```

## Deployment 📦
The project use CI/CD to run unit tests and next deploy to provider cloud, current the deploys just happen in master branch and production environment.

## Api Docs
You can check the api documentation here: 
https://documenter.getpostman.com/view/3417767/TzY1gbSB 

