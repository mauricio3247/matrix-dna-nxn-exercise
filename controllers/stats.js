const express = require('express')
const router = express.Router()
const { DNA_MUTATION_EVALUATOR_SERVICE } = require('../services/dna-mutation-evaluator.service')
const DNA_REGISTER_SERVICE = require('../services/dna-register.service')
router.get('/stats', async (req, res, next)=> {
    try {
        const response = await DNA_REGISTER_SERVICE.getStats()
        return res.json({...response})
    } catch (error) {
        return res.status(400).end()
    }
})

module.exports = router