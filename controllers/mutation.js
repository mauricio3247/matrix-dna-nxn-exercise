const express = require('express')
const router = express.Router()
const { DNA_MUTATION_EVALUATOR_SERVICE } = require('../services/dna-mutation-evaluator.service')
const DNA_REGISTER_SERVICE = require('../services/dna-register.service')
router.post('/mutation', async (req, res, next)=> {
    try {
        const {dna} = req.body
        const response = DNA_MUTATION_EVALUATOR_SERVICE.hasMutation(dna)
        await DNA_REGISTER_SERVICE.create(dna, response)
        if(response) {
            return res.status(200).end()
        }
        return res.status(403).end()
    } catch (error) {
        return res.status(404).end()
    }
})

module.exports = router