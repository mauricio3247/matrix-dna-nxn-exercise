const {checkIsMatrixNxN} = require('../helpers/check-matrix-nxn')
const {checkOnlyIsHaveOnlyCharactersAllowed} = require('../helpers/check-matrix-have-char-allowed')
const {DNAMutationEvaluator} = require('./dna-mutation-evaluator')
const COINCIDENCES_NUMBER = 4
const letter = ['A','T', 'C','G']
class DNAMutationEvaluatorService {
    /**
     * 
     * @param {string[]} matrix 
     * @returns {boolean}
     */
    hasMutation (matrix) {
        if(checkIsMatrixNxN(matrix) === false) {
            return false
        }

        if(checkOnlyIsHaveOnlyCharactersAllowed(matrix, letter) === false) {
            return false
        }

        const evaluator = new DNAMutationEvaluator(matrix, COINCIDENCES_NUMBER)
        return evaluator.hasMutation()
    }
}

module.exports = {
    DNA_MUTATION_EVALUATOR_SERVICE: new DNAMutationEvaluatorService()
}