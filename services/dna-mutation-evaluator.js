const LEFT = 'left'
const RIGHT = 'right'

/**
 * Class to evaluate if a matrix NxN have or not mutation 
*/
class DNAMutationEvaluator {
    /**
     * Must be works only with matrix NxN also we can set the coincideces number to check and compare if the matrix have a mutation
     * @param {string[]} matrix 
     * @param {number} coincidencesNumberToCheckMutation 
     */
    constructor (matrix, coincidencesNumberToCheckMutation) {
        this._matrix = matrix.map(item => item.split(','))
        this._matrixLength = this._matrix.length //only needs determinate the matrix longitude once time
        this._coincidencesNumber = coincidencesNumberToCheckMutation
    }


    /**
     * Evaluate if a row have an empty value or the value is diferent to matrix item point value (checking) then restart the counter and update the new value
     * if the matrix item value is equal to commonItem row value then just increase the counter to be close the get a coincidence number
     * @param {value: string, count: number} commonItemX 
     * @param {number} indX 
     * @param {number} indY
     * 
     * @returns {
     *  value: string,
     *  count: number
     * }
     */
    _evaluateRow (commonItemX, indX, indY)  {
        if(commonItemX.value === '' || commonItemX.value !== this._matrix[indX][indY]) {
            return {value: this._matrix[indX][indY], count: 1}
        } else if(commonItemX.value === this._matrix[indX][indY]) {
            return {...commonItemX, count: commonItemX.count + 1}
        } 

    }

    /**
     * Evaluate is a column have an indexX 0 to init the object, other wise check if the value of columnCommon object is equal to matrix item point 
     * to increase the counter value, in case that the values don't match then restart the counter and set a new value.
     * @param {value: string, count: number} commonItemY 
     * @param {number} indX 
     * @param {number} indY
     * 
     * @returns {
     *  value: string,
     *  count: number
     * }
     */
    _evaluateColumn (commonItemY, indX, indY) {
        const value = this._matrix[indX][indY]
        if(indX === 0 || commonItemY === undefined) {
            return {value: value, count:1}
        } else if(commonItemY.value === value) {
            return {...commonItemY, count: commonItemY.count + 1}
        } else {
            return {value: value, count:1}
        }
    }

    /**
     * For cases where exist an entry point to init a diagonal, we need to initialize origin points and track every steps to considerate every before point 
     * in ther order: x-1, y+-1 (y+1 to check for the left to right and y-1 to check for the right to left), so we can evaluate in a point x,y if the befores points 
     * have a same value or we need to restart de counter
     * 
     * @param {number} indX 
     * @param {number} indY 
     * @returns {
     * {
     *      origin: {x: number, y: number}, 
     *      left: {value: string: count: number, beforePoint: {x: number, y: number}} | undefined,  
     *      right: {value: string: count: number, beforePoint: {x: number, y: number}} | undefined, 
     * } | undefined
     * }
     */
    _initDiagonalItems (indX, indY) {
        const value = this._matrix[indX][indY]
        const origin = { x: indX, y: indY }
        const beforePoint = { x: indX, y: indY }
        const response = {
            origin,
            left: {value, count:1, beforePoint },
            right: {value, count:1, beforePoint },
        }

        if(indY === 0) {
            return {...response, left: undefined }
        }

        if (indY === this._matrixLength - 1) {
            return {...response, right: undefined}
        }

        if(indX===0) {
            return response    
        }
    }

    /**
     * Allow to evaluate if an item have any coincidences when is compare to their diagonal side tracker,
     * first we need to find the index of tracker object in the side an diagonal coords
     * next we compare if the value of item point match with the common diagonal value and increase the counter coincidences to their diagonal,
     * other wise if the values dont match the value common is updated and the counter is restarted
     *  
     * @param {Map<string, {
     *      origin: {x: number, y: number}, 
     *      left: {value: string: count: number, beforePoint: {x: number, y: number}} | undefined,  
     *      right: {value: string: count: number, beforePoint: {x: number, y: number}} | undefined, 
     * }>} commonItemsXY 
     * @param {LEFT | RIGHT} side 
     * @param {number} indX 
     * @param {number} indY 
     * @returns  {
     * Map<string, {
     *      origin: {x: number, y: number}, 
     *      left: {value: string: count: number, beforePoint: {x: number, y: number}} | undefined,  
     *      right: {value: string: count: number, beforePoint: {x: number, y: number}} | undefined, 
     * }> | boolean
     * }
     */
    _evaluateSide (commonItemsXY, side, indX, indY) {
        const value = this._matrix[indX][indY]
        const beforePoint = { x: indX, y: indY }
        const indexKey = Array.from(commonItemsXY.keys()).find(itemCommon => {
            const item = commonItemsXY.get(itemCommon)
            const variableIndex = side === LEFT ? indY + 1 : indY - 1
            if(item[side] && item[side].beforePoint.x === indX - 1 &&  item[side].beforePoint.y === variableIndex) {
                return true
            }
        })


        if (indexKey) {
            const finded = commonItemsXY.get(indexKey)
            if(finded[side].value === value) {
                commonItemsXY.set(indexKey, {...finded, [side]: {...finded[side], count: finded[side].count + 1, beforePoint }})
                if(commonItemsXY.get(indexKey)[side].count >= this._coincidencesNumber) {
                    return true
                }
            } else {
                commonItemsXY.set(indexKey, {...finded, [side]: {...finded[side], count: 1, beforePoint, value }})
            }
        }

        return commonItemsXY
    }


    /**
     * Check if the matrix has or not mutations, to knows that, we need to determine if we can find the N characters in sequence (XXXX), this sequence can be
     * in Row, Columns or Diagonal
     * @returns boolean
     */
    hasMutation () {
        if(this._matrixLength < this._coincidencesNumber) {
            return false
        }
        let commonItemsY = []
        let commonItemsXY = new Map()
        for (let indX = 0; indX < this._matrixLength; indX++) {
            let commonItemX = {value:'', count:0}
            
            for (let indY = 0; indY < this._matrixLength; indY ++) {
                commonItemX = this._evaluateRow(commonItemX, indX, indY )
                if(commonItemX.count === this._coincidencesNumber) {
                    return true
                }
                commonItemsY[indY] = this._evaluateColumn(commonItemsY[indY], indX, indY)
                if(commonItemsY[indY].count === this._coincidencesNumber) {
                    return true
                }

                const response = this._initDiagonalItems (indX, indY)
                if(response !== undefined) {
                    commonItemsXY.set(`${indX}-${indY}`, response)
                }

                commonItemsXY = this._evaluateSide(commonItemsXY, LEFT, indX, indY)
                if(commonItemsXY === true) {
                    return true
                }
                commonItemsXY = this._evaluateSide(commonItemsXY, RIGHT, indX, indY)
                if(commonItemsXY === true) {
                    return true
                }

            }
        }

        return false
    }
}

module.exports = {
    DNAMutationEvaluator
}