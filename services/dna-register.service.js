const crypto = require('crypto')
const {DnaRegister} = require('../models/dna-register.model')


class DnaRegisterService {


    _generateHash(matrix) {
        try {
            const hash=  crypto.createHash('sha256').update(JSON.stringify(matrix)).digest('hex');
            return hash
        } catch (error) {
            throw new Error('Error to try generate hash')
        }

    }

    async _findByMatrix (hash) {
        return DnaRegister.findOne({dna_hash: hash })
    }

    async create (matrix, hasMutation) {
        try {
            const hash = this._generateHash(matrix)
            const exist = await this._findByMatrix(hash)
            if(exist === null || exist === undefined) {
                await DnaRegister.create({
                    dna_hash: hash,
                    has_mutation: hasMutation
                })
                return true
            }
            return false
        } catch (error) {
            throw new Error('Error to try generate hash to save')
        }
    }

    async getStats () {
        try {
            let response = {count_mutations: 0, count_no_mutation: 0, ratio: 0} 
            const result = await DnaRegister
            .aggregate([
                {
                    $group: {
                        _id: {$cond: ["$has_mutation", "count_mutations", "count_no_mutation"]},
                        count: {
                            $sum: 1
                        },
                    }
                }
              ]);
              if(result) {
                response = Object.keys(result).reduce ((acc, item) => {
                    return {...acc, [result[item]._id]: result[item].count}
                }, response)
              }

              if(response.count_no_mutation !== 0) {
                  response.ratio = response.count_mutations / response.count_no_mutation
              }

              return response
        } catch (error) {
            console.log('error', error)
            throw new Error('Error to try get stats')
        }

    }
}

module.exports = new DnaRegisterService()