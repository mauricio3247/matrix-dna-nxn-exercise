const chai = require('chai');
const assert = chai.assert
const { DNAMutationEvaluator  } = require('../services/dna-mutation-evaluator')
const { MATRIX_OK_MOCKS } = require('../mocks/matrix-mutation-ok');
const { MATRIX_WRONG_MOCKS } = require('../mocks/matrix-mutation-wrong');
const COINCIDENCES_NUMBER = 4 

describe('Test to DNAMutationEvaluator Class', function() {


    describe('#_evaluateRow()', function() {
      
      it('check that 0,0 point have a init value and counter 1', function(done) {
        const commonItemX = {value: '', count: 0}
        const response = (new DNAMutationEvaluator(MATRIX_OK_MOCKS.MATRIX_4X4_ROW_MATCH, COINCIDENCES_NUMBER))._evaluateRow(commonItemX, 0, 0)
        assert.ok(response, 'Must be return something')
        assert.isObject(response, 'Must be an oject')
        assert.equal(response.value, 'A', 'value must be equal to A')
        assert.equal(response.count, 1, 'count must be equal to 1')
        done()
      })

      it('check that 0,1 point can increase the counter by 1 and preserve the value', function(done) {
        const commonItemX = {value: 'A', count: 1}
        const response = (new DNAMutationEvaluator(MATRIX_OK_MOCKS.MATRIX_4X4_ROW_MATCH, COINCIDENCES_NUMBER))._evaluateRow(commonItemX, 0, 1)
        assert.ok(response, 'Must be return something')
        assert.isObject(response, 'Must be an oject')
        assert.equal(response.value, 'A', 'value must be equal to A')
        assert.equal(response.count, 2, 'count must be equal to 2')
        done()
      })

      it('check that 1,1 point can restart the counter in 1 and change the value', function(done) {
        const commonItemX = {value: 'C', count: 1}
        const response = (new DNAMutationEvaluator(MATRIX_OK_MOCKS.MATRIX_4X4_ROW_MATCH, COINCIDENCES_NUMBER))._evaluateRow(commonItemX, 1, 1)
        assert.ok(response, 'Must be return something')
        assert.isObject(response, 'Must be an oject')
        assert.notEqual(response.value, 'C', 'value must not be equal to C')
        assert.equal(response.value, 'A', 'value must be equal to A')
        assert.equal(response.count, 1, 'count must be equal to 1')
        done()
      })

    })

    describe('#_evaluateColumn()', function() {
      
      it('check that 0,0 point have a init value and counter 1', function(done) {
        const commonItemY = undefined
        const response = (new DNAMutationEvaluator(MATRIX_OK_MOCKS.MATRIX_4X4_ROW_MATCH, COINCIDENCES_NUMBER))._evaluateColumn(commonItemY, 0, 0)
        assert.ok(response, 'Must be return something')
        assert.isObject(response, 'Must be an oject')
        assert.equal(response.value, 'A', 'value must be equal to A')
        assert.equal(response.count, 1, 'count must be equal to 1')
        done()
      })

      it('check that if the commonItemY is undefined start an object to track the column common items', function(done) {
        const commonItemY = undefined
        const response = (new DNAMutationEvaluator(MATRIX_OK_MOCKS.MATRIX_4X4_ROW_MATCH, COINCIDENCES_NUMBER))._evaluateColumn(commonItemY, 1, 0)
        assert.ok(response, 'Must be return something')
        assert.isObject(response, 'Must be an oject')
        assert.equal(response.value, 'C', 'value must be equal to C')
        assert.equal(response.count, 1, 'count must be equal to 1')
        done()
      })

      it('check that 1,0 point have can restart the counter in 1 and change the value', function(done) {
        const commonItemY = {value: 'A', count: 1}
        const response = (new DNAMutationEvaluator(MATRIX_OK_MOCKS.MATRIX_4X4_ROW_MATCH, COINCIDENCES_NUMBER))._evaluateColumn(commonItemY, 1, 0)
        assert.ok(response, 'Must be return something')
        assert.isObject(response, 'Must be an oject')
        assert.notEqual(response.value, 'A', 'value must not be equal to A')
        assert.equal(response.value, 'C', 'value must be equal to C')
        assert.equal(response.count, 1, 'count must be equal to 1')
        done()
      })

    })

    describe('#_initDiagonalItems()', function() {
      
      it('check that 0,0 point return an object without left property ', function(done) {
        const response = (new DNAMutationEvaluator(MATRIX_OK_MOCKS.MATRIX_4X4_ROW_MATCH, COINCIDENCES_NUMBER))._initDiagonalItems(0,0)
        assert.ok(response, 'Must be return something')
        assert.isObject(response, 'Must be an oject')
        assert.isUndefined(response.left, 'Left must be undefined')
        assert.isDefined(response.right, 'Right must be defined')
        done()
      })

      it('check that 0,1 point return an object with left and right properties ', function(done) {
        const response = (new DNAMutationEvaluator(MATRIX_OK_MOCKS.MATRIX_4X4_ROW_MATCH, COINCIDENCES_NUMBER))._initDiagonalItems(0,1)
        assert.ok(response, 'Must be return something')
        assert.isObject(response, 'Must be an oject')
        assert.isDefined(response.left, 'Left must be defined')
        assert.isDefined(response.right, 'Right must be defined')
        done()
      })

      it('check that 0,3 point return an object without right property ', function(done) {
        const response = (new DNAMutationEvaluator(MATRIX_OK_MOCKS.MATRIX_4X4_ROW_MATCH, COINCIDENCES_NUMBER))._initDiagonalItems(0,3)
        assert.ok(response, 'Must be return something')
        assert.isObject(response, 'Must be an oject')
        assert.isDefined(response.left, 'Left must be defined')
        assert.isUndefined(response.right, 'Right must be undefined')
        done()
      })


      it('check that 1,2 point return undefined ', function(done) {
        const response = (new DNAMutationEvaluator(MATRIX_OK_MOCKS.MATRIX_4X4_ROW_MATCH, COINCIDENCES_NUMBER))._initDiagonalItems(1,2)
        assert.isUndefined(response, 'The response must be undefined')
        done()
      })
    })


    describe('#_evaluateSide()', function() {
      const LEFT = 'left'
      const RIGHT = 'right'

      
      it('check that 1,1 can be related with the diagonalItesm in position 0,0 when is evaluate for right side', function(done) {
        const evaluator = new DNAMutationEvaluator(MATRIX_OK_MOCKS.MATRIX_4X4_ROW_MATCH, COINCIDENCES_NUMBER)
        const commonItemsXY = new Map()
        const data = evaluator._initDiagonalItems(0,0)
        if(data !== undefined) {
          commonItemsXY.set(`0-0`, data)
        }
        evaluator._evaluateSide(commonItemsXY, RIGHT , 1,1)
        const response = commonItemsXY.get(`0-0`)
        assert.ok(response, 'Must be return something')
        assert.isObject(response, 'Must be an oject')
        assert.isUndefined(response.left, 'Left must be undefined')
        assert.isDefined(response.right, 'Right must be defined')
        assert.isDefined(response.origin, 'Origin must be defined')
        assert.equal(response.origin.x, 0, 'Origin X must be 0')
        assert.equal(response.origin.y, 0, 'Origin Y must be 0')

        assert.equal(response.right.value,'A', 'Must be equal A')
        assert.equal(response.right.count, 2 , 'Must be equal 2')
        done()
      })

    })

    describe('#hasMutation()', function() {

      it('matrix 4x4 with one ROW AAAA (first row), must be return true', function(done) {
        const response = (new DNAMutationEvaluator(MATRIX_OK_MOCKS.MATRIX_4X4_ROW_MATCH, COINCIDENCES_NUMBER)).hasMutation()
        assert.ok(response)
        done()
      });


      it('matrix 4x4 with one Column TTTT (3rd column), must be return true', function(done) {
        const response = (new DNAMutationEvaluator(MATRIX_OK_MOCKS.MATRIX_4X4_COLUMN_MATCH, COINCIDENCES_NUMBER)).hasMutation()
        assert.ok(response)
        done()
      });

      
      it('matrix 4x4 with one main diagonal TTTT, must be return true', function(done) {
        const response = (new DNAMutationEvaluator(MATRIX_OK_MOCKS.MATRIX_4X4_MAIN_DIAGONAL_MATCH, COINCIDENCES_NUMBER)).hasMutation()
        assert.ok(response)
        done()
      });

      it('matrix 4x4 with one second diagonal AAAA, must be return true', function(done) {
        const response = (new DNAMutationEvaluator(MATRIX_OK_MOCKS.MATRIX_4X4_MAIN_DIAGONAL_MATCH, COINCIDENCES_NUMBER)).hasMutation()
        assert.ok(response)
        done()
      });

      it('matrix 6x6 with ROW TTTT (3rd row), must be return true', function(done) {
        const response = (new DNAMutationEvaluator(MATRIX_OK_MOCKS.MATRIX_6X6_ROW_MATCH, COINCIDENCES_NUMBER)).hasMutation()
        assert.ok(response)
        done()
      });

      it('matrix 6x6 with Column AAAA (5TH column), must be return true', function(done) {
        const response = (new DNAMutationEvaluator(MATRIX_OK_MOCKS.MATRIX_6X6_COLUMN_MATCH, COINCIDENCES_NUMBER)).hasMutation()
        assert.ok(response)
        done()
      });

      it('matrix 6x6 with Diagonal CCCC (1,0 - 4,3), must be return true', function(done) {
        const response = (new DNAMutationEvaluator(MATRIX_OK_MOCKS.MATRIX_6X6_DIAGONAL_10_43_MATCH, COINCIDENCES_NUMBER)).hasMutation()
        assert.ok(response)
        done()
      });


      it('matrix 6x6 with Diagonal GGGG 4,2 - 5,1), must be return true', function(done) {
        const response = (new DNAMutationEvaluator(MATRIX_OK_MOCKS.MATRIX_6X6_DIAGONAL_42_51_MATCH, COINCIDENCES_NUMBER)).hasMutation()
        assert.ok(response)
        done()
      });

      it('matrix 4x4 without sequecens that match with coincidencesNumber, must be return false', function(done) {
        const response = (new DNAMutationEvaluator(MATRIX_WRONG_MOCKS.MATRIX_4X4_WRONG, COINCIDENCES_NUMBER)).hasMutation()
        assert.notOk(response)
        done()
      });

      it('matrix 6x6 without sequecens that match with coincidencesNumber, must be return false', function(done) {
        const response = (new DNAMutationEvaluator(MATRIX_WRONG_MOCKS.MATRIX_6X6_WRONG, COINCIDENCES_NUMBER)).hasMutation()
        assert.notOk(response)
        done()
      });

    });
  });