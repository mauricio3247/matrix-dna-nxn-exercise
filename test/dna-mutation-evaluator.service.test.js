const chai = require('chai');
const assert = chai.assert
const { DNA_MUTATION_EVALUATOR_SERVICE  } = require('../services/dna-mutation-evaluator.service')
const { MATRIX_OK_MOCKS } = require('../mocks/matrix-mutation-ok');
const { MATRIX_WRONG_MOCKS } = require('../mocks/matrix-mutation-wrong');


describe('Test to DNA_MUTATION_EVALUATOR_SERVICE', function() {
    describe('#hasMutation()', function() {

        it('matrix 4x4 with one ROW AAAA (first row), must be return true', function(done) {
            const response = DNA_MUTATION_EVALUATOR_SERVICE.hasMutation(MATRIX_OK_MOCKS.MATRIX_4X4_ROW_MATCH)
            assert.ok(response)
            done()
        });
  
  
        it('matrix 4x4 with one Column TTTT (3rd column), must be return true', function(done) {
            const response = DNA_MUTATION_EVALUATOR_SERVICE.hasMutation(MATRIX_OK_MOCKS.MATRIX_4X4_COLUMN_MATCH)
            assert.ok(response)
            done()
        });
  
        
        it('matrix 4x4 with one main diagonal TTTT, must be return true', function(done) {
            const response = DNA_MUTATION_EVALUATOR_SERVICE.hasMutation(MATRIX_OK_MOCKS.MATRIX_4X4_MAIN_DIAGONAL_MATCH)
          assert.ok(response)
          done()
        });
  
        it('matrix 4x4 with one second diagonal AAAA, must be return true', function(done) {
            const response = DNA_MUTATION_EVALUATOR_SERVICE.hasMutation(MATRIX_OK_MOCKS.MATRIX_4X4_MAIN_DIAGONAL_MATCH)

            assert.ok(response)
            done()
        });
  
        it('matrix 6x6 with ROW TTTT (3rd row), must be return true', function(done) {
            const response = DNA_MUTATION_EVALUATOR_SERVICE.hasMutation(MATRIX_OK_MOCKS.MATRIX_6X6_ROW_MATCH)
            assert.ok(response)
            done()
        });
  
        it('matrix 6x6 with Column AAAA (5TH column), must be return true', function(done) {
            const response = DNA_MUTATION_EVALUATOR_SERVICE.hasMutation(MATRIX_OK_MOCKS.MATRIX_6X6_COLUMN_MATCH)
            assert.ok(response)
            done()
        });
  
        it('matrix 6x6 with Diagonal CCCC (1,0 - 4,3), must be return true', function(done) {
            const response = DNA_MUTATION_EVALUATOR_SERVICE.hasMutation(MATRIX_OK_MOCKS.MATRIX_6X6_DIAGONAL_10_43_MATCH)
            assert.ok(response)
            done()
        });
  
  
        it('matrix 6x6 with Diagonal GGGG 4,2 - 5,1), must be return true', function(done) {
            const response = DNA_MUTATION_EVALUATOR_SERVICE.hasMutation(MATRIX_OK_MOCKS.MATRIX_6X6_DIAGONAL_42_51_MATCH)
            assert.ok(response)
            done()
        });
  
        it('matrix 4x4 without sequecens that match with coincidencesNumber, must be return false', function(done) {
            const response = DNA_MUTATION_EVALUATOR_SERVICE.hasMutation(MATRIX_WRONG_MOCKS.MATRIX_4X4_WRONG)
            assert.notOk(response)
            done()
        });
  
        it('matrix 6x6 without sequecens that match with coincidencesNumber, must be return false', function(done) {
            const response = DNA_MUTATION_EVALUATOR_SERVICE.hasMutation(MATRIX_WRONG_MOCKS.MATRIX_6X6_WRONG)
            assert.notOk(response)
            done()
        });
  
      });
})  