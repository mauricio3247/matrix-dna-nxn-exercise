const app = require('./app')
app.listen(process.env.PORT || 3000, ()=> {
    console.log('Server Running  at ', process.env.PORT || 3000)
})
