const mongoose = require('mongoose')

const schemaDnaRegister = new mongoose.Schema({
    dna_hash: {type: String, index: true},
    has_mutation: {type: Boolean, index: true}
},{ schema: 'dna-register'})

const DnaRegister = mongoose.model('DnaRegister', schemaDnaRegister);
module.exports = {
    DnaRegister
}