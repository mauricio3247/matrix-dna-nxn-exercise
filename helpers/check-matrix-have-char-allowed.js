const checkOnlyIsHaveOnlyCharactersAllowed = (matrix, CHARACTERS) => {
    matrix.forEach (itemRow => {
        itemRow.split(',').forEach(item => {
            if(CHARACTERS.find(itemChar => itemChar === item) === undefined) {
                return false
            }
        })
    })

    return true
}

module.exports = {
    checkOnlyIsHaveOnlyCharactersAllowed
}