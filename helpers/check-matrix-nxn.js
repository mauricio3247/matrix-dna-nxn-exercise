/**
 * Check is matrix have the same length in rows and columns
 * @param {string[]} matrix 
 * @returns {boolean}
 */
const checkIsMatrixNxN = (matrix) => {
    const limit = matrix.length
    matrix.forEach (item => {
        if (item.split(',').length !== limit) {
            return false
        }
    })
    return true
}

module.exports = {
    checkIsMatrixNxN
}