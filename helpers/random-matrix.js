const letter = ['A','T', 'C','G']

const generateRandomIntegerNumber = (start, limit) => {
    return Math.floor(Math.random() * (limit- (start - 1))) + start;
}

const generateLetterDna = () => letter[generateRandomIntegerNumber(1,4) - 1]

const generateMatrix2D = (min, max) => {
    const limitMatrix = generateRandomIntegerNumber(min, max)
    const matrix = []
    for (let i =0 ; i < limitMatrix; i++) {
        const row = []
        for (let j=0; j < limitMatrix; j++) {
            row.push(generateLetterDna())
        }
        matrix.push(row.join(','))
    }
    return matrix
}

module.exports = {
    generateMatrix2D
}

